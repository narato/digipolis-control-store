﻿cd $env:USERPROFILE\Source\Repos\digipolis\digipolis-control-store\ControlStoreApi.UnitTests
&"$env:USERPROFILE\.nuget\packages\OpenCover\4.6.519\tools\OpenCover.Console.exe" "-target:${env:programfiles}\dotnet\dotnet.exe" "-targetargs: test" -register:"User" "-filter:+[Digipolis.SurveyComposer.*]* -[*.Test]* -[*.DataProvider]*.Migrations.* -[*]*.Program -[*]*.Startup -[xunit.*]*" -output:"Unit_Test_Coverage_Report.xml" -oldStyle


&"$env:USERPROFILE\.nuget\packages\ReportGenerator\2.5.2\tools\ReportGenerator.exe" -reports:"$env:USERPROFILE\Source\Repos\digipolis\digipolis-control-store\ControlStoreApi.UnitTests\Unit_Test_Coverage_Report.xml" -targetdir:"$env:USERPROFILE\Source\Repos\digipolis\digipolis-control-store\OverallTestReport\UnitTestCoverage_Report"

cd ..