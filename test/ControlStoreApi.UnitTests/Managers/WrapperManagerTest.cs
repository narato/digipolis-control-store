﻿using Digipolis.SurveyComposer.ControlStore.Common.SystemWrappers.Interfaces;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Domain.Managers;
using Microsoft.Net.Http.Headers;
using Moq;
using Narato.Common.Exceptions;
using Xunit;

namespace ControlStoreApi.UnitTests.Managers
{
    public class WrapperManagerTest
    {
        [Fact]
        public void GetNonExistingWrapperByNameTest()
        {
            var wrapperDataProviderMock = new Mock<IWrapperDataProvider>();
            wrapperDataProviderMock.Setup(wdpm => wdpm.WrapperDirectoryExists("test")).Returns(false);

            var wrapperManager = new WrapperManager(wrapperDataProviderMock.Object);

            Assert.ThrowsAsync<EntityNotFoundException>(() => wrapperManager.GetWrapperByNameAsync("test"));
        }

        [Fact]
        public void GetWrapperByNameTest()
        {
            var fileInfoMock = new Mock<IFileInfo>();
            fileInfoMock.Setup(fim => fim.GetByteContentAsync()).ReturnsAsync(new byte[0]);
            fileInfoMock.Setup(fim => fim.GetContentType()).Returns(new MediaTypeHeaderValue("text/html"));

            var wrapperDataProviderMock = new Mock<IWrapperDataProvider>();
            wrapperDataProviderMock.Setup(wdpm => wdpm.WrapperDirectoryExists("test")).Returns(true);
            wrapperDataProviderMock.Setup(wdpm => wdpm.getWrapperFileByWrapperName("test")).Returns(fileInfoMock.Object);

            var wrapperManager = new WrapperManager(wrapperDataProviderMock.Object);

            Assert.ThrowsAsync<EntityNotFoundException>(() => wrapperManager.GetWrapperByNameAsync("test"));
        }
    }
}
