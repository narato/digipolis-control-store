﻿using AutoMapper;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Domain.Managers;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Moq;
using Narato.Common.Exceptions;
using Newtonsoft.Json.Linq;
using System;
using Xunit;

namespace ControlStoreApi.UnitTests.Managers
{
    public class ControlSetManagerTest
    {
        [Fact]
        public void GetControlSetByNameTest()
        {
            var rawControlSet = new JArray();
            var controlSet = new ControlSet();
            var controlSetDataProviderMock = new Mock<IControlSetDataProvider>();
            controlSetDataProviderMock.Setup(csdpm => csdpm.GetControlSetByNameAsync(It.IsAny<string>())).ReturnsAsync(controlSet);

            var controlSetManager = new ControlSetManager(controlSetDataProviderMock.Object);

            var result = controlSetManager.GetControlSetByNameAsync("test").Result;

            Assert.Equal(result, controlSet);
        }

        [Fact]
        public void UpdateNonExistingControlSetShouldThrowExceptionTest()
        {
            var guid = Guid.NewGuid();

            var controlSetDataProviderMock = new Mock<IControlSetDataProvider>();
            controlSetDataProviderMock.Setup(csdpm => csdpm.ControlSetExistsByName("original")).ReturnsAsync(false);
            controlSetDataProviderMock.Setup(csdpm => csdpm.GetControlSetByIdAsync(guid)).ThrowsAsync(new EntityNotFoundException("Controlset with name original doesn't exist"));

            var controlSetManager = new ControlSetManager(controlSetDataProviderMock.Object);

            
            var exception = Assert.ThrowsAsync<EntityNotFoundException>(() => controlSetManager.UpdateControlSetAsync(new ControlSet() { Id = guid }, guid));

            Assert.Equal(exception.Result.Message, "Controlset with name original doesn't exist");
        }

        [Fact]
        public void UpdateControlSetTest()
        {
            var guid = Guid.NewGuid();
            var newControlSet = new ControlSet() { Name = "new", Id = guid };
            var insertedControlSet = new ControlSet() { Name = "new", Id = guid };
            var controlSetDataProviderMock = new Mock<IControlSetDataProvider>();
            controlSetDataProviderMock.Setup(csdpm => csdpm.GetControlSetByIdAsync(guid)).ReturnsAsync(insertedControlSet);
            controlSetDataProviderMock.Setup(csdpm => csdpm.ControlSetExistsByName("original")).ReturnsAsync(true);
            controlSetDataProviderMock.Setup(csdpm => csdpm.ControlSetExistsByName("new")).ReturnsAsync(false);
            controlSetDataProviderMock.Setup(csdpm => csdpm.InsertControlSetAsync(newControlSet)).ReturnsAsync(insertedControlSet);
            controlSetDataProviderMock.Setup(csdpm => csdpm.GetControlSetByNameAsync("new")).ReturnsAsync(insertedControlSet);

            var controlSetManager = new ControlSetManager(controlSetDataProviderMock.Object);

            var result = controlSetManager.UpdateControlSetAsync(newControlSet, guid).Result;

            Assert.Equal(result, insertedControlSet);
        }

        [Fact]
        public void InsertAlreadyExistingControlSetShouldThrowExceptionTest()
        {
            var controlSetDataProviderMock = new Mock<IControlSetDataProvider>();
            controlSetDataProviderMock.Setup(csdpm => csdpm.ControlSetExistsByName("new")).ReturnsAsync(true);

            var controlSetManager = new ControlSetManager(controlSetDataProviderMock.Object);

            var exception = Assert.ThrowsAsync<ValidationException>(() => controlSetManager.InsertControlSetAsync(new ControlSet() { Name = "new" }));

            Assert.Equal(exception.Result.Feedback.Count, 1);
        }

        [Fact]
        public void InsertControlSetThatFailsShouldThrowExceptionTest()
        {
            var controlSetDataProviderMock = new Mock<IControlSetDataProvider>();
            controlSetDataProviderMock.Setup(csdpm => csdpm.ControlSetExistsByName("new")).ReturnsAsync(false);
            controlSetDataProviderMock.Setup(csdpm => csdpm.InsertControlSetAsync(It.IsAny<ControlSet>())).ReturnsAsync(new ControlSet());

            var controlSetManager = new ControlSetManager(controlSetDataProviderMock.Object);

            Assert.ThrowsAsync<ExceptionWithFeedback>(() => controlSetManager.InsertControlSetAsync(new ControlSet() { Name = "new" }));
        }
    }
}
