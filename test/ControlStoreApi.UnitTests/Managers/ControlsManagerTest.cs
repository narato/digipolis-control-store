﻿using AutoMapper;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Domain.Managers;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Moq;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ControlStoreApi.UnitTests.Managers
{
    public class ControlsManagerTest
    {
        [Fact]
        public void GetLatestControlsMetadataTest()
        {
            var controlDataProviderMock = new Mock<IControlDataProvider>();
            controlDataProviderMock.Setup(cdpm => cdpm.GetLatestControlMetadataAsync()).ReturnsAsync(new List<ControlMetadata>());

            var controlSetManagerMock = new Mock<IControlSetManager>();

            var controlsManager = new ControlsManager(controlDataProviderMock.Object, controlSetManagerMock.Object);

            var result = controlsManager.GetLatestControlsMetadataAsync().Result;

            Assert.NotNull(result);
            Assert.Equal(result.Count(), 0);

            controlDataProviderMock.Verify(cdpm => cdpm.GetLatestControlMetadataAsync(), Times.Once);
        }

        [Fact]
        public void GetControlsMetadataByControlSetTest()
        {
            var controlSet = new ControlSet()
            {
                Name = "OOR",
                Controls = new List<ControlDefinition>()
                {
                   new ControlDefinition()
                   {
                       Name = "test",
                       Version = "filledIn"
                   },
                   new ControlDefinition()
                   {
                       Name = "test2"
                   }
                }
            };

            var versionedControlMetadata = new ControlMetadata();
            var nonVersionedControlMetadata = new ControlMetadata();

            var controlDataProviderMock = new Mock<IControlDataProvider>();
            controlDataProviderMock.Setup(cdpm => cdpm.GetLatestControlMetadataByNameAsync("test2")).ReturnsAsync(nonVersionedControlMetadata);
            controlDataProviderMock.Setup(cdpm => cdpm.GetControlMetadataByNameAndVersionAsync("test", "filledIn")).ReturnsAsync(versionedControlMetadata);

            var controlSetManagerMock = new Mock<IControlSetManager>();
            controlSetManagerMock.Setup(csmm => csmm.GetControlSetByNameAsync("OOR")).ReturnsAsync(controlSet);

            var controlsManager = new ControlsManager(controlDataProviderMock.Object, controlSetManagerMock.Object);

            var result = controlsManager.GetControlsMetadataByControlSetAsync("OOR").Result;

            Assert.NotNull(result);
            Assert.Equal(result.Count(), 2);

            controlDataProviderMock.Verify(cdpm => cdpm.GetLatestControlMetadataByNameAsync("test2"), Times.Once);
            controlDataProviderMock.Verify(cdpm => cdpm.GetControlMetadataByNameAndVersionAsync("test", "filledIn"), Times.Once);
        }

        [Fact]
        public void GetLatestControlMetadataByNameTest()
        {

            var controlDataProviderMock = new Mock<IControlDataProvider>();
            controlDataProviderMock.Setup(cdpm => cdpm.GetLatestControlMetadataByNameAsync("test")).ReturnsAsync(new ControlMetadata());

            var controlSetManagerMock = new Mock<IControlSetManager>();

            var controlsManager = new ControlsManager(controlDataProviderMock.Object, controlSetManagerMock.Object);

            var result = controlsManager.GetLatestControlMetadataByNameAsync("test").Result;

            Assert.NotNull(result);

            controlDataProviderMock.Verify(cdpm => cdpm.GetLatestControlMetadataByNameAsync("test"), Times.Once);
        }

        [Fact]
        public void GetLatestControlMetadataByNameAndVersionTest()
        {
            var controlDataProviderMock = new Mock<IControlDataProvider>();
            controlDataProviderMock.Setup(cdpm => cdpm.GetControlMetadataByNameAndVersionAsync("test", "1")).ReturnsAsync(new ControlMetadata());

            var controlSetManagerMock = new Mock<IControlSetManager>();

            var controlsManager = new ControlsManager(controlDataProviderMock.Object, controlSetManagerMock.Object);

            var result = controlsManager.GetControlMetadataByNameAndVersionAsync("test", "1").Result;

            Assert.NotNull(result);

            controlDataProviderMock.Verify(cdpm => cdpm.GetControlMetadataByNameAndVersionAsync("test", "1"), Times.Once);
        }

        [Fact]
        public void GetAllControlMetadataVersionsByNameTest()
        {
            var controlDataProviderMock = new Mock<IControlDataProvider>();
            controlDataProviderMock.Setup(cdpm => cdpm.GetAllControlMetadataVersionsByNameAsync("test")).ReturnsAsync(new List<ControlMetadata>());

            var controlSetManagerMock = new Mock<IControlSetManager>();

            var controlsManager = new ControlsManager(controlDataProviderMock.Object, controlSetManagerMock.Object);

            var result = controlsManager.GetAllControlMetadataVersionsByNameAsync("test").Result;

            Assert.NotNull(result);

            controlDataProviderMock.Verify(cdpm => cdpm.GetAllControlMetadataVersionsByNameAsync("test"), Times.Once);
        }
    }
}
