﻿using Digipolis.SurveyComposer.ControlStore.DataProvider.Sorting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ControlStoreApi.UnitTests.Sorting
{
    public class NaturalSortingTest
    {
        [Fact]
        public void NaturalOrderByTest()
        {
            IEnumerable<string> list = new List<string>()
            {
                "1.0.0",
                "2.0.0",
                "10.0.0"
            };

            list = list.OrderBy(i => i);
            Assert.Equal(list.First(), "1.0.0");
            Assert.Equal(list.Last(), "2.0.0");

            list = list.NaturalOrderBy(i => i);
            Assert.Equal(list.First(), "1.0.0");
            Assert.Equal(list.Last(), "10.0.0");
        }

        [Fact]
        public void NaturalOrderByDescendingTest()
        {
            IEnumerable<string> list = new List<string>()
            {
                "1.0.0",
                "2.0.0",
                "10.0.0"
            };

            list = list.OrderByDescending(i => i);
            Assert.Equal(list.First(), "2.0.0");
            Assert.Equal(list.Last(), "1.0.0");

            list = list.NaturalOrderByDescending(i => i);
            Assert.Equal(list.First(), "10.0.0");
            Assert.Equal(list.Last(), "1.0.0");
        }
    }
}
