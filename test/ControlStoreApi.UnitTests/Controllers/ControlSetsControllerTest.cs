﻿using ControlStoreApi.Controllers;
using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.Factory;
using System;
using System.Threading.Tasks;
using Xunit;

namespace ControlStoreApi.UnitTests.Controllers
{
    public class ControlSetsControllerTest
    {
        [Fact]
        public void GetByNameReturnsAnActionResult()
        {
            var controlsetsManagerMock = new Mock<IControlSetManager>(MockBehavior.Strict);
            var factoryMock = new Mock<IResponseFactory>(MockBehavior.Strict);

            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<ControlSet>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));
            var controller = new ControlSetsController(factoryMock.Object, controlsetsManagerMock.Object);

            IActionResult actionResult = controller.GetByNameAsync("bla").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void PostReturnsAnActionResult()
        {
            var controlsetsManagerMock = new Mock<IControlSetManager>(MockBehavior.Strict);
            var factoryMock = new Mock<IResponseFactory>(MockBehavior.Strict);

            factoryMock.Setup(fm => fm.CreatePostResponseAsync(It.IsAny<Func<Task<ControlSet>>>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Func<ControlSet, object>>())).ReturnsAsync(new ObjectResult(null));
            var controller = new ControlSetsController(factoryMock.Object, controlsetsManagerMock.Object);

            IActionResult actionResult = controller.PostAsync(new ControlSet()).Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void PutReturnsAnActionResult()
        {
            var controlsetsManagerMock = new Mock<IControlSetManager>(MockBehavior.Strict);
            var factoryMock = new Mock<IResponseFactory>(MockBehavior.Strict);

            factoryMock.Setup(fm => fm.CreatePutResponseAsync(It.IsAny<Func<Task<ControlSet>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));
            var controller = new ControlSetsController(factoryMock.Object, controlsetsManagerMock.Object);

            IActionResult actionResult = controller.PutAsync(new ControlSet(), Guid.NewGuid()).Result;

            Assert.NotNull(actionResult);
        }
    }
}
