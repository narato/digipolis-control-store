﻿using ControlStoreApi.Controllers;
using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.Interfaces;
using System;
using Xunit;

namespace ControlStoreApi.UnitTests.Controllers
{
    public class WrappersControllerTest
    {
        [Fact]
        public void GetByNameReturnsAFileContentResult()
        {
            var wrapperManagerMock = new Mock<IWrapperManager>(MockBehavior.Strict);
            wrapperManagerMock.Setup(fmm => fmm.GetWrapperByNameAsync(It.IsAny<string>())).ReturnsAsync(new FileContentResult(new byte[0], "text/csv"));
            var exceptionToActionResultMapperMock = new Mock<IExceptionToActionResultMapper>(MockBehavior.Strict);

            var controller = new WrappersController(wrapperManagerMock.Object, exceptionToActionResultMapperMock.Object);

            IActionResult actionResult = controller.GetWrapperByName("bla").Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(FileContentResult), actionResult);
        }

        [Fact]
        public void GetByNameThatThrowsReturnsAMappedObjectResult()
        {
            var wrapperManagerMock = new Mock<IWrapperManager>(MockBehavior.Strict);
            wrapperManagerMock.Setup(fmm => fmm.GetWrapperByNameAsync(It.IsAny<string>())).Throws(new Exception());
            var exceptionToActionResultMapperMock = new Mock<IExceptionToActionResultMapper>(MockBehavior.Strict);
            exceptionToActionResultMapperMock.Setup(etarmm => etarmm.Map(It.IsAny<Exception>(), It.IsAny<string>())).Returns(new ObjectResult(null));

            var controller = new WrappersController(wrapperManagerMock.Object, exceptionToActionResultMapperMock.Object);

            IActionResult actionResult = controller.GetWrapperByName("bla").Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(ObjectResult), actionResult);
        }
    }
}
