﻿using ControlStoreApi.Controllers;
using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.Factory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ControlStoreApi.UnitTests.Controllers
{
    public class ControlsControllerTest
    {
        [Fact]
        public void GetAllReturnsAnActionResult()
        {
            var controlsManagerMock = new Mock<IControlsManager>(MockBehavior.Strict);
            var factoryMock = new Mock<IResponseFactory>(MockBehavior.Strict);

            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<IEnumerable<ControlMetadata>>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));
            var controller = new ControlsController(controlsManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAllAsync(null).Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void GetAllWithControlSetNameReturnsAnActionResult()
        {
            var controlsManagerMock = new Mock<IControlsManager>(MockBehavior.Strict);
            var factoryMock = new Mock<IResponseFactory>(MockBehavior.Strict);

            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<IEnumerable<ControlMetadata>>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));
            var controller = new ControlsController(controlsManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAllAsync("bla").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void GetLatestByNameReturnsAnActionResult()
        {
            var controlsManagerMock = new Mock<IControlsManager>(MockBehavior.Strict);
            var factoryMock = new Mock<IResponseFactory>(MockBehavior.Strict);

            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<ControlMetadata>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));
            var controller = new ControlsController(controlsManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetLatestByKeyAsync("bla").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void GetAllVersionsByNameReturnsAnActionResult()
        {
            var controlsManagerMock = new Mock<IControlsManager>(MockBehavior.Strict);
            var factoryMock = new Mock<IResponseFactory>(MockBehavior.Strict);

            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<IDictionary<string, ControlMetadata>>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));
            var controller = new ControlsController(controlsManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAllVersionsByKeyAsync("bla").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void GetByVersionAndNameReturnsAnActionResult()
        {
            var controlsManagerMock = new Mock<IControlsManager>(MockBehavior.Strict);
            var factoryMock = new Mock<IResponseFactory>(MockBehavior.Strict);

            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<ControlMetadata>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));
            var controller = new ControlsController(controlsManagerMock.Object, factoryMock.Object);


            IActionResult actionResult = controller.GetControlByKeyAndSemanticVersionAsync("bla", "1.0.1").Result;

            Assert.NotNull(actionResult);
        }
    }
}
