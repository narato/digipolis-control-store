﻿using Digipolis.SurveyComposer.ControlStore.Common.SystemWrappers.Interfaces;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Contexts;
using Digipolis.SurveyComposer.ControlStore.DataProvider.DataProviders;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Microsoft.EntityFrameworkCore;
using Moq;
using Narato.Common.Exceptions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ControlStoreApi.UnitTests.DataProviders
{
    public class ControlDataProviderTest
    {

        private ControlMetadata GetControlMetaDataMock(string version = "1.0.0")
        {
            var control = new ControlMetadata();
            control.DesigntimeMetadata = new DesigntimeMetadata()
            {
                Description = "Checkbox Control",
                Logo = "",
                Name = "checkbox",
                Version = version,
                Properties = new List<PropertyMetadata>()
            };
            control.DesigntimeMetadata.Properties.Add(new PropertyMetadata()
            {
                Name = "checkbox",
                Type = "checkbox",
                Description = "the checkbox"
            });
            control.RuntimeMetadata = new RuntimeMetadata()
            {
                Name = "Checkbox Control",
                Key = "checkbox",
                Version = version,
                Wrapper = "default",
                Files = new FilesMetadata()
            };
            return control;
        }

        [Fact]
        public void GetLatestControlMetadataAsyncTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ControlStoreContext>();
            optionsBuilder.UseInMemoryDatabase();

            var context = new ControlStoreContext(optionsBuilder.Options);
            context.ControlMetadata.RemoveRange(context.ControlMetadata.ToList());
            context.SaveChanges();

            context.ControlMetadata.Add(GetControlMetaDataMock());
            context.ControlMetadata.Add(GetControlMetaDataMock("1.0.1"));
            context.SaveChanges();

            var controlDataProvider = new ControlDataProvider(context);

            var result = controlDataProvider.GetLatestControlMetadataAsync().Result;

            Assert.Equal(result.Count(), 1);
        }

        [Fact]
        public void GetLatestControlMetadataByNameThrowsExceptionIfNotFoundTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ControlStoreContext>();
            optionsBuilder.UseInMemoryDatabase();

            var context = new ControlStoreContext(optionsBuilder.Options);

            var controlDataProvider = new ControlDataProvider(context);

            Assert.ThrowsAsync<EntityNotFoundException>(() => controlDataProvider.GetLatestControlMetadataByNameAsync("test"));
        }

        [Fact]
        public void GetLatestControlMetadataByNameTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ControlStoreContext>();
            optionsBuilder.UseInMemoryDatabase();

            var context = new ControlStoreContext(optionsBuilder.Options);
            context.ControlMetadata.RemoveRange(context.ControlMetadata.ToList());
            context.SaveChanges();

            context.ControlMetadata.Add(GetControlMetaDataMock());
            context.SaveChanges();

            var controlDataProvider = new ControlDataProvider(context);

            var result = controlDataProvider.GetLatestControlMetadataByNameAsync("checkbox").Result;

            Assert.NotNull(result);
            Assert.Equal("checkbox", result.RuntimeMetadata.Key);
        }

        [Fact]
        public void GetControlMetadataByNameAndVersionThrowsExceptionIfNotFoundTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ControlStoreContext>();
            optionsBuilder.UseInMemoryDatabase();

            var context = new ControlStoreContext(optionsBuilder.Options);
            context.ControlMetadata.RemoveRange(context.ControlMetadata.ToList());
            context.SaveChanges();

            context.ControlMetadata.Add(GetControlMetaDataMock());
            context.SaveChanges();

            var controlDataProvider = new ControlDataProvider(context);

            Assert.ThrowsAsync<EntityNotFoundException>(() => controlDataProvider.GetControlMetadataByNameAndVersionAsync("checkbox", "2.0.0"));
        }

        [Fact]
        public void GetControlMetadataByNameAndVersionTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ControlStoreContext>();
            optionsBuilder.UseInMemoryDatabase();

            var context = new ControlStoreContext(optionsBuilder.Options);
            context.ControlMetadata.RemoveRange(context.ControlMetadata.ToList());
            context.SaveChanges();

            context.ControlMetadata.Add(GetControlMetaDataMock());
            context.SaveChanges();

            var controlDataProvider = new ControlDataProvider(context);

            var result = controlDataProvider.GetControlMetadataByNameAndVersionAsync("checkbox", "1.0.0").Result;

            Assert.NotNull(result);
        }

        [Fact]
        public void GetAllControlMetadataVersionsByNameTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ControlStoreContext>();
            optionsBuilder.UseInMemoryDatabase();

            var context = new ControlStoreContext(optionsBuilder.Options);
            context.ControlMetadata.RemoveRange(context.ControlMetadata.ToList());
            context.SaveChanges();

            context.ControlMetadata.Add(GetControlMetaDataMock());
            context.ControlMetadata.Add(GetControlMetaDataMock("1.0.1"));
            context.SaveChanges();

            var controlDataProvider = new ControlDataProvider(context);

            var result = controlDataProvider.GetAllControlMetadataVersionsByNameAsync("checkbox").Result;

            Assert.NotNull(result);
            Assert.Equal(result.Count(), 2);
        }
    }
}
