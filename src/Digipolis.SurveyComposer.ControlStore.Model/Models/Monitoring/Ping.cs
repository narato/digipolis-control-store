﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.Model.Models.Monitoring
{
    public class Ping
    {
        public string Status { get; set; }
    }
}
