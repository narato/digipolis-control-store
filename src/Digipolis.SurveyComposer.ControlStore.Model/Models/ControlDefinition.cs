﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Digipolis.SurveyComposer.ControlStore.Model.Models
{
    public class ControlDefinition
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Version { get; set; }

        [JsonIgnore]
        public Guid ControlSetId { get; set; }
        [JsonIgnore]
        public ControlSet ControlSet { get; set; }
    }
}
