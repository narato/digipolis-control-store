﻿using System;
using System.Collections.Generic;

namespace Digipolis.SurveyComposer.ControlStore.Model.Models
{
    public class ControlSet
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<ControlDefinition> Controls { get; set; }
    }
}
