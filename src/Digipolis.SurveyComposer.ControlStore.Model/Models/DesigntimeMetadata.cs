﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Digipolis.SurveyComposer.ControlStore.Model.Models
{
    public class DesigntimeMetadata
    {
        public Guid Id { get; set; }
        public string Logo { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
        public ICollection<PropertyMetadata> Properties { get; set; }

        [JsonIgnore]
        public ControlMetadata ControlMetadata { get; set; }
    }
}
