﻿using Digipolis.SurveyComposer.ControlStore.DataProvider.Contexts;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.DataProvider.QueryProviders
{
    public class QueryExecutor : IQueryExecutor
    {
        private readonly ControlStoreContext _context;
        private readonly IMigrationQueryProvider _queryProvider;

        public QueryExecutor(ControlStoreContext context, IMigrationQueryProvider queryProvider)
        {
            _context = context;
            _queryProvider = queryProvider;
        }

        public int ExecuteNonQuery(NpgsqlCommand command)
        {
            return command.ExecuteNonQuery();
        }

        public int CreateDatabase(string connectionString, string databaseName)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    var createCommand = connection.CreateCommand();
                    createCommand.CommandText = _queryProvider.GetCreateDbQuery(databaseName);
                    return createCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public object FindDatabase(string connectionString, string databaseName)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    string cmdText = _queryProvider.GetDbExistsQuery(databaseName);
                    var cmd = connection.CreateCommand();
                    cmd.CommandText = cmdText;
                    var db = cmd.ExecuteScalar();
                    return db;
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string MigrateDatabase()
        {
            _context.Database.Migrate();
            return "Migrations successfully executed";
        }
    }
}
