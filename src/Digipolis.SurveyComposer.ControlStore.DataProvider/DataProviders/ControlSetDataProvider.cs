﻿using Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using System.Threading.Tasks;
using NLog;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Contexts;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Narato.Common.Exceptions;
using System;
using System.Collections.Generic;

namespace Digipolis.SurveyComposer.ControlStore.DataProvider.DataProviders
{
    public class ControlSetDataProvider : IControlSetDataProvider
    {
        private readonly ControlStoreContext _context;
        protected static Logger Logger = LogManager.GetCurrentClassLogger();

        public ControlSetDataProvider(ControlStoreContext context)
        {
            _context = context;
        }

        public async Task<bool> ControlSetExistsByName(string name)
        {
            return (await _context.ControlSets
                .Where(cs => cs.Name == name)
                .CountAsync()) > 0;
        }

        public async Task<ControlSet> InsertControlSetAsync(ControlSet controlSet)
        {
            _context.ControlSets.Add(controlSet);
            await _context.SaveChangesAsync();
            return controlSet;
        }

        public async Task<ControlSet> UpdateControlSetAsync(ControlSet controlSet)
        {
            _context.Update(controlSet);
            await _context.SaveChangesAsync();
            return controlSet;
        }

        public async Task<ControlSet> FindControlSetByNameAsync(string name)
        {
            return await _context.ControlSets
                .Where(cs => cs.Name == name)
                .Include(cs => cs.Controls)
                .FirstOrDefaultAsync();
        }

        public async Task<ControlSet> GetControlSetByNameAsync(string name)
        {
            var controlset = await FindControlSetByNameAsync(name);

            if (controlset == null)
            {
                throw new EntityNotFoundException($"Controlset with name {name} doesn't exist.");
            }
            return controlset;
        }

        public async Task<ControlSet> GetControlSetByIdAsync(Guid id)
        {
            var controlset = await _context.ControlSets
                .Where(cs => cs.Id == id)
                .Include(cs => cs.Controls)
                .FirstOrDefaultAsync();

            if (controlset == null)
            {
                throw new EntityNotFoundException($"Controlset with id {id} doesn't exist.");
            }
            return controlset;
        }

        public async Task<bool> DeleteControlSetById(Guid id)
        {
            var controlSet = await GetControlSetByIdAsync(id);

            _context.ControlSets.Remove(controlSet);
            return await _context.SaveChangesAsync() > 0;
        }

        // Basically it checks if any of the Controls' id's exist already under another controlset
        public async Task<IEnumerable<ControlDefinition>> GetControlsAlreadyInOtherControlSets(ControlSet controlset)
        {
            var controlIdList = controlset.Controls.Select(cd => cd.Id);
            return await _context.ControlDefinitions
                .Where(cd => cd.ControlSetId != controlset.Id && controlIdList.Contains(cd.Id))
                .ToListAsync();
        }
    }
}
