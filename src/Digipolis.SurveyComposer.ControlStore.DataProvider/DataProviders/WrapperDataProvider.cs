﻿using Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces;
using System.IO;
using Digipolis.SurveyComposer.ControlStore.Common.Extensions;
using Digipolis.SurveyComposer.ControlStore.Common.SystemWrappers.Interfaces;

namespace Digipolis.SurveyComposer.ControlStore.DataProvider.DataProviders
{
    public class WrapperDataProvider : IWrapperDataProvider
    {
        public IFileInfo getWrapperFileByWrapperName(string name)
        {
            var wrapperDirectory = new DirectoryInfo(Directory.GetCurrentDirectory() + @"/Files/wrappers/" + name);
            return wrapperDirectory.GetFileFromDirectoryByName("wrapper.html");
        }

        public bool WrapperDirectoryExists(string name)
        {
            var wrapperDirectory = new DirectoryInfo(Directory.GetCurrentDirectory() + @"/Files/wrappers/" + name);
            return wrapperDirectory.Exists;
        }
    }
}
