﻿using Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using Narato.Common.Exceptions;
using System.Linq;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Contexts;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

namespace Digipolis.SurveyComposer.ControlStore.DataProvider.DataProviders
{
    public class ControlDataProvider : IControlDataProvider
    {
        private readonly ControlStoreContext _context;

        public ControlDataProvider(ControlStoreContext context)
        {
            _context = context;
        }

        private IQueryable<ControlMetadata> GetBaseQuery()
        {
            return _context.ControlMetadata
                .Include(cm => cm.DesigntimeMetadata)
                .Include(cm => cm.RuntimeMetadata)
                .Include(cm => cm.DesigntimeMetadata.Properties)
                .Include(cm => cm.RuntimeMetadata.Files)
                .Include(cm => cm.RuntimeMetadata.Files.Code);
        }

        public async Task<IEnumerable<ControlMetadata>> GetLatestControlMetadataAsync()
        {
            var controls = await GetBaseQuery()
                .OrderByDescending(cm => cm.RuntimeMetadata.Version)
                .ToListAsync();
            
            return controls.GroupBy(cm => cm.RuntimeMetadata.Name) // grouBy currently not supported by EntityFrameworkCore
                .Select(group => group.First());

        }

        public async Task<ControlMetadata> InsertControlMetadataAsync(ControlMetadata controlMetaData)
        {
            _context.ControlMetadata.Add(controlMetaData);
            await _context.SaveChangesAsync();
            return controlMetaData;
        }

        public async Task<ControlMetadata> GetLatestControlMetadataByNameAsync(string name)
        {
            var controlMetadata = await GetBaseQuery()
                .Where(cm => cm.RuntimeMetadata.Key == name)
                .OrderByDescending(cm => cm.RuntimeMetadata.Version)
                .FirstOrDefaultAsync();

            if (controlMetadata == null)
                throw new EntityNotFoundException("no entry could be found for the latest version of control: " + name);
            return controlMetadata;
        }

        public async Task<ControlMetadata> FindControlMetadataByNameAndVersionAsync(string name, string version)
        {
            return await GetBaseQuery()
                .Where(cm => cm.RuntimeMetadata.Key == name && cm.RuntimeMetadata.Version == version)
                .FirstOrDefaultAsync();
        }

        public async Task<ControlMetadata> FindControlMetadataByNameAndSemanticVersionAsync(string name, string version)
        {
            return await GetBaseQuery()
                .Where(cm => cm.RuntimeMetadata.Key == name)
                .OrderByDescending(cm => cm.RuntimeMetadata.Version)
                .FirstOrDefaultAsync(cm => cm.RuntimeMetadata.Version.StartsWith(version + '.'));
        }        

        public async Task<ControlMetadata> GetControlMetadataByNameAndVersionAsync(string name, string version)
        {
            var controlMetadata = await FindControlMetadataByNameAndVersionAsync(name, version);

            if (controlMetadata == null)
                throw new EntityNotFoundException($"no entry could be found for version {version} of control: {name}");
            return controlMetadata;
        }

        public async Task<ControlMetadata> GetControlMetadataByNameAndSemanticVersionAsync(string name, string version)
        {
            if (new Regex(@"^\d+\.\d+\.\d+$").IsMatch(version))
            {
                return await GetControlMetadataByNameAndVersionAsync(name, version);
            }
            else
            {
                var controlMetadata = await FindControlMetadataByNameAndSemanticVersionAsync(name, version);

                if (controlMetadata == null)
                    throw new EntityNotFoundException($"no entry could be found for version {version} of control: {name}");
                return controlMetadata;
            }
        }

        public async Task<IEnumerable<ControlMetadata>> GetAllControlMetadataVersionsByNameAsync(string name)
        {
            return await GetBaseQuery()
                .Where(cm => cm.RuntimeMetadata.Key == name)
                .ToListAsync();
        }
    }
}
