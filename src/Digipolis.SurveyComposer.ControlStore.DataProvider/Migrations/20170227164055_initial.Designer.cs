﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Contexts;

namespace Digipolis.SurveyComposer.ControlStore.DataProvider.Migrations
{
    [DbContext(typeof(ControlStoreContext))]
    [Migration("20170227164055_initial")]
    partial class initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:PostgresExtension:.uuid-ossp", "'uuid-ossp', '', ''")
                .HasAnnotation("ProductVersion", "1.0.1");

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.ControlDefinition", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("ControlSetId");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Version");

                    b.HasKey("Id");

                    b.HasIndex("ControlSetId");

                    b.ToTable("ControlDefinitions");
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.ControlSet", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("ControlSets");
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.ControlDefinition", b =>
                {
                    b.HasOne("Digipolis.SurveyComposer.ControlStore.Model.Models.ControlSet", "ControlSet")
                        .WithMany("Controls")
                        .HasForeignKey("ControlSetId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
