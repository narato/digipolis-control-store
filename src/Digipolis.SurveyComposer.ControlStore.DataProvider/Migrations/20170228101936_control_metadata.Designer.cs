﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Contexts;

namespace Digipolis.SurveyComposer.ControlStore.DataProvider.Migrations
{
    [DbContext(typeof(ControlStoreContext))]
    [Migration("20170228101936_control_metadata")]
    partial class control_metadata
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:PostgresExtension:.uuid-ossp", "'uuid-ossp', '', ''")
                .HasAnnotation("ProductVersion", "1.0.1");

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.ControlDefinition", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("ControlSetId");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Version");

                    b.HasKey("Id");

                    b.HasIndex("ControlSetId");

                    b.ToTable("ControlDefinitions");
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.ControlMetadata", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("DesigntimeMetadataId");

                    b.Property<Guid>("RuntimeMetadataId");

                    b.HasKey("Id");

                    b.HasIndex("DesigntimeMetadataId")
                        .IsUnique();

                    b.HasIndex("RuntimeMetadataId")
                        .IsUnique();

                    b.ToTable("ControlMetadata");
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.ControlSet", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("ControlSets");
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.DesigntimeMetadata", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("Logo");

                    b.Property<string>("Name");

                    b.Property<string>("Version");

                    b.HasKey("Id");

                    b.ToTable("DesigntimeMetadata");
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.FilePath", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("FilesMetadataId");

                    b.Property<string>("Path");

                    b.HasKey("Id");

                    b.HasIndex("FilesMetadataId");

                    b.ToTable("FilePaths");
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.FilesMetadata", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Styles");

                    b.Property<string>("Template");

                    b.HasKey("Id");

                    b.ToTable("FilesMetadata");
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.PropertyMetadata", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<Guid>("DesigntimeMetadataId");

                    b.Property<string>("Name");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.HasIndex("DesigntimeMetadataId");

                    b.ToTable("PropertyMetadata");
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.RuntimeMetadata", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("FilesId");

                    b.Property<string>("Key");

                    b.Property<string>("Name");

                    b.Property<string>("Version");

                    b.Property<string>("Wrapper");

                    b.HasKey("Id");

                    b.HasIndex("FilesId")
                        .IsUnique();

                    b.ToTable("RuntimeMetadata");
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.ControlDefinition", b =>
                {
                    b.HasOne("Digipolis.SurveyComposer.ControlStore.Model.Models.ControlSet", "ControlSet")
                        .WithMany("Controls")
                        .HasForeignKey("ControlSetId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.ControlMetadata", b =>
                {
                    b.HasOne("Digipolis.SurveyComposer.ControlStore.Model.Models.DesigntimeMetadata", "DesigntimeMetadata")
                        .WithOne("ControlMetadata")
                        .HasForeignKey("Digipolis.SurveyComposer.ControlStore.Model.Models.ControlMetadata", "DesigntimeMetadataId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Digipolis.SurveyComposer.ControlStore.Model.Models.RuntimeMetadata", "RuntimeMetadata")
                        .WithOne("ControlMetadata")
                        .HasForeignKey("Digipolis.SurveyComposer.ControlStore.Model.Models.ControlMetadata", "RuntimeMetadataId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.FilePath", b =>
                {
                    b.HasOne("Digipolis.SurveyComposer.ControlStore.Model.Models.FilesMetadata", "FilesMetadata")
                        .WithMany("Code")
                        .HasForeignKey("FilesMetadataId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.PropertyMetadata", b =>
                {
                    b.HasOne("Digipolis.SurveyComposer.ControlStore.Model.Models.DesigntimeMetadata", "DesigntimeMetadata")
                        .WithMany("Properties")
                        .HasForeignKey("DesigntimeMetadataId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Digipolis.SurveyComposer.ControlStore.Model.Models.RuntimeMetadata", b =>
                {
                    b.HasOne("Digipolis.SurveyComposer.ControlStore.Model.Models.FilesMetadata", "Files")
                        .WithOne("RuntimeMetadata")
                        .HasForeignKey("Digipolis.SurveyComposer.ControlStore.Model.Models.RuntimeMetadata", "FilesId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
