﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Digipolis.SurveyComposer.ControlStore.DataProvider.Migrations
{
    public partial class control_metadata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ControlDefinitions_ControlSets_ControlSetId",
                table: "ControlDefinitions");

            migrationBuilder.CreateTable(
                name: "DesigntimeMetadata",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Description = table.Column<string>(nullable: true),
                    Logo = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DesigntimeMetadata", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FilesMetadata",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Styles = table.Column<string>(nullable: true),
                    Template = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilesMetadata", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PropertyMetadata",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Description = table.Column<string>(nullable: true),
                    DesigntimeMetadataId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyMetadata", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PropertyMetadata_DesigntimeMetadata_DesigntimeMetadataId",
                        column: x => x.DesigntimeMetadataId,
                        principalTable: "DesigntimeMetadata",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FilePaths",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    FilesMetadataId = table.Column<Guid>(nullable: false),
                    Path = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilePaths", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FilePaths_FilesMetadata_FilesMetadataId",
                        column: x => x.FilesMetadataId,
                        principalTable: "FilesMetadata",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RuntimeMetadata",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    FilesId = table.Column<Guid>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true),
                    Wrapper = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuntimeMetadata", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RuntimeMetadata_FilesMetadata_FilesId",
                        column: x => x.FilesId,
                        principalTable: "FilesMetadata",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ControlMetadata",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DesigntimeMetadataId = table.Column<Guid>(nullable: false),
                    RuntimeMetadataId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ControlMetadata", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ControlMetadata_DesigntimeMetadata_DesigntimeMetadataId",
                        column: x => x.DesigntimeMetadataId,
                        principalTable: "DesigntimeMetadata",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ControlMetadata_RuntimeMetadata_RuntimeMetadataId",
                        column: x => x.RuntimeMetadataId,
                        principalTable: "RuntimeMetadata",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.AlterColumn<Guid>(
                name: "ControlSetId",
                table: "ControlDefinitions",
                nullable: false);

            migrationBuilder.CreateIndex(
                name: "IX_ControlMetadata_DesigntimeMetadataId",
                table: "ControlMetadata",
                column: "DesigntimeMetadataId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ControlMetadata_RuntimeMetadataId",
                table: "ControlMetadata",
                column: "RuntimeMetadataId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FilePaths_FilesMetadataId",
                table: "FilePaths",
                column: "FilesMetadataId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyMetadata_DesigntimeMetadataId",
                table: "PropertyMetadata",
                column: "DesigntimeMetadataId");

            migrationBuilder.CreateIndex(
                name: "IX_RuntimeMetadata_FilesId",
                table: "RuntimeMetadata",
                column: "FilesId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ControlDefinitions_ControlSets_ControlSetId",
                table: "ControlDefinitions",
                column: "ControlSetId",
                principalTable: "ControlSets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ControlDefinitions_ControlSets_ControlSetId",
                table: "ControlDefinitions");

            migrationBuilder.DropTable(
                name: "ControlMetadata");

            migrationBuilder.DropTable(
                name: "FilePaths");

            migrationBuilder.DropTable(
                name: "PropertyMetadata");

            migrationBuilder.DropTable(
                name: "RuntimeMetadata");

            migrationBuilder.DropTable(
                name: "DesigntimeMetadata");

            migrationBuilder.DropTable(
                name: "FilesMetadata");

            migrationBuilder.AlterColumn<Guid>(
                name: "ControlSetId",
                table: "ControlDefinitions",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ControlDefinitions_ControlSets_ControlSetId",
                table: "ControlDefinitions",
                column: "ControlSetId",
                principalTable: "ControlSets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
