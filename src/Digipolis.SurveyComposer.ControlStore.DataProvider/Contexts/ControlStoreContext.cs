﻿using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore.PostgreSQL;

namespace Digipolis.SurveyComposer.ControlStore.DataProvider.Contexts
{
    public class ControlStoreContext : DbContext
    {
        public DbSet<ControlSet> ControlSets { get; set; }
        public DbSet<ControlDefinition> ControlDefinitions { get; set; }

        public DbSet<ControlMetadata> ControlMetadata { get; set; }
        public DbSet<DesigntimeMetadata> DesigntimeMetadata { get; set; }
        public DbSet<RuntimeMetadata> RuntimeMetadata { get; set; }
        public DbSet<FilesMetadata> FilesMetadata { get; set; }
        public DbSet<PropertyMetadata> PropertyMetadata { get; set; }
        public DbSet<FilePath> FilePaths { get; set; }

        private string _connectionString;

        public ControlStoreContext()
        {
            _connectionString = "Server = 192.168.99.100; Port = 5432; Database = ControlStore; User Id = postgres; Password = narato;";
        }

        public ControlStoreContext(DbContextOptions<ControlStoreContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasPostgresExtension("uuid-ossp");

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_connectionString != null)
            {
                optionsBuilder.UseNpgsql(_connectionString);
                return;
            }
            base.OnConfiguring(optionsBuilder);
        }
    }
}
