﻿namespace Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces
{
    public interface IQueryExecutor
    {
        object FindDatabase(string connectionString, string databaseName);
        int CreateDatabase(string connectionString, string databaseName);
        string MigrateDatabase();
    }
}
