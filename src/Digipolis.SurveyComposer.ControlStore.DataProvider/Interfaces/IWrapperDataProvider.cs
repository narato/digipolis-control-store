﻿using Digipolis.SurveyComposer.ControlStore.Common.SystemWrappers.Interfaces;

namespace Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces
{
    public interface IWrapperDataProvider
    {
        bool WrapperDirectoryExists(string name);
        IFileInfo getWrapperFileByWrapperName(string name);
    }
}
