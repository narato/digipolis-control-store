﻿using Digipolis.SurveyComposer.ControlStore.Model.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces
{
    public interface IControlSetDataProvider
    {
        Task<bool> ControlSetExistsByName(string name);
        Task<ControlSet> InsertControlSetAsync(ControlSet controlSet);
        Task<ControlSet> UpdateControlSetAsync(ControlSet controlSet);
        Task<ControlSet> FindControlSetByNameAsync(string name);
        Task<ControlSet> GetControlSetByNameAsync(string name);
        Task<ControlSet> GetControlSetByIdAsync(Guid id);
        Task<bool> DeleteControlSetById(Guid id);

        Task<IEnumerable<ControlDefinition>> GetControlsAlreadyInOtherControlSets(ControlSet controlset);
    }
}
