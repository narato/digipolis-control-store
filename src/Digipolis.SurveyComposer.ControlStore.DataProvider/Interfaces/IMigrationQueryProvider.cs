﻿namespace Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces
{
    public interface IMigrationQueryProvider
    {
        string GetDbExistsQuery(string dbName);
        string GetCreateDbQuery(string dbName);
    }
}
