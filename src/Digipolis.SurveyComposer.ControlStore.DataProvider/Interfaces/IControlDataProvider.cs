﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Digipolis.SurveyComposer.ControlStore.Model.Models;

namespace Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces
{
    public interface IControlDataProvider
    {
        Task<IEnumerable<ControlMetadata>> GetLatestControlMetadataAsync();

        Task<ControlMetadata> InsertControlMetadataAsync(ControlMetadata controlMetaData);

        Task<ControlMetadata> GetLatestControlMetadataByNameAsync(string name);

        Task<ControlMetadata> FindControlMetadataByNameAndVersionAsync(string name, string version);

        Task<ControlMetadata> GetControlMetadataByNameAndSemanticVersionAsync(string name, string version);

        Task<ControlMetadata> GetControlMetadataByNameAndVersionAsync(string name, string version);

        Task<IEnumerable<ControlMetadata>> GetAllControlMetadataVersionsByNameAsync(string name);
    }
}
