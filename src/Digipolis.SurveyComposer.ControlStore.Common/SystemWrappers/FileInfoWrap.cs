﻿using Digipolis.SurveyComposer.ControlStore.Common.Extensions;
using Digipolis.SurveyComposer.ControlStore.Common.SystemWrappers.Interfaces;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Net.Http.Headers;

namespace Digipolis.SurveyComposer.ControlStore.Common.SystemWrappers
{
    public class FileInfoWrap : IFileInfo
    {
        private readonly FileInfo _fileInfo;

        public FileInfoWrap(FileInfo fileInfo)
        {
            _fileInfo = fileInfo;
        }

        public async Task<byte[]> GetByteContentAsync()
        {
            return await _fileInfo.GetByteContentAsync();
        }

        public MediaTypeHeaderValue GetContentType()
        {
            return _fileInfo.GetContentType();
        }

        public async Task WriteTextAsync(string text)
        {
            await _fileInfo.WriteTextAsync(text);
        }
    }
}
