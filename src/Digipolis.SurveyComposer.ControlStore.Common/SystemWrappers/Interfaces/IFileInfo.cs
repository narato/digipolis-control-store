﻿using Microsoft.Net.Http.Headers;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.Common.SystemWrappers.Interfaces
{
    public interface IFileInfo
    {
        MediaTypeHeaderValue GetContentType();
        Task<byte[]> GetByteContentAsync();
        Task WriteTextAsync(string text);
    }
}
