﻿namespace Digipolis.SurveyComposer.ControlStore.Common.Configurations
{
    public class DatabaseConfiguration
    {
        public string ConnectionString { get; set; }
    }
}
