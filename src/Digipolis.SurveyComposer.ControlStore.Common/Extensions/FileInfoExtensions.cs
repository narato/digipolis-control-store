﻿using Microsoft.Net.Http.Headers;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.StaticFiles;
using System.Text;

namespace Digipolis.SurveyComposer.ControlStore.Common.Extensions
{
    public static class FileInfoExtensions
    {
        public static MediaTypeHeaderValue GetContentType(this FileInfo fileInfo)
        {
            string contentType;
            new FileExtensionContentTypeProvider().TryGetContentType(fileInfo.FullName, out contentType);
            return MediaTypeHeaderValue.Parse(contentType ?? "application/octet-stream");
        }

        public static async Task<byte[]> GetByteContentAsync(this FileInfo fileInfo)
        {
            byte[] result;
            using (FileStream stream = fileInfo.OpenRead())
            {
                result = new byte[stream.Length];
                await stream.ReadAsync(result, 0, (int)stream.Length);
            }
            return result;
        }

        public static async Task WriteTextAsync(this FileInfo fileInfo, string text)
        {
            using (FileStream stream = fileInfo.OpenWrite())
            {
                byte[] textBytes = Encoding.UTF8.GetBytes(text);
                await stream.WriteAsync(textBytes, 0, textBytes.Length);
            }
        }
    }
}
