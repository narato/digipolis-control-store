﻿using Digipolis.SurveyComposer.ControlStore.Common.SystemWrappers;
using Digipolis.SurveyComposer.ControlStore.Common.SystemWrappers.Interfaces;
using Narato.Common.Exceptions;
using System.IO;
using System.Linq;

namespace Digipolis.SurveyComposer.ControlStore.Common.Extensions
{
    public static class DirectoryInfoExtensions
    {
        public static IFileInfo GetFileFromDirectoryByName(this DirectoryInfo directoryInfo, string name)
        {
            var fileInfo = directoryInfo.GetFiles()
                .Where(f => f.Name == name)
                .FirstOrDefault();
            if (fileInfo == null || !fileInfo.Exists)
            {
                throw new EntityNotFoundException($"The file {name} couldn't be found within {directoryInfo}");
            }
            return new FileInfoWrap(fileInfo);
        }
    }
}
