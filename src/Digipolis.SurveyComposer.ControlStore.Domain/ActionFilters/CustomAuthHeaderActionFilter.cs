﻿using Digipolis.SurveyComposer.ControlStore.Common.Configurations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;

namespace Digipolis.SurveyComposer.ControlStore.Domain.ActionFilters
{
    public class CustomAuthHeaderActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            // agreed, this is a pretty sucky way to do DI, but hey, it's a temp solution in its whole :/
            IOptions<ControlStoreConfiguration> controlStoreConfig = (IOptions<ControlStoreConfiguration>)context.HttpContext.RequestServices.GetService(typeof(IOptions<ControlStoreConfiguration>));
            if (context.HttpContext.Request.Headers["X-Custom-Auth"].ToString() != controlStoreConfig.Value.AuthHeaderValue)
            {
                context.Result = new UnauthorizedResult();
            }
        }
    }
}
