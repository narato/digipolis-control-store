﻿using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using AutoMapper;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces;
using System.Linq;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Managers
{
    public class ControlsManager : IControlsManager
    {
        private readonly IControlDataProvider _controlsDataProvider;
        private readonly IControlSetManager _controlSetManager;

        public ControlsManager(IControlDataProvider controlsDataProvider, IControlSetManager controlSetManager)
        {
            _controlsDataProvider = controlsDataProvider;
            _controlSetManager = controlSetManager;
        }

        public async Task<IEnumerable<ControlMetadata>> GetLatestControlsMetadataAsync()
        {
            return await _controlsDataProvider.GetLatestControlMetadataAsync();
        }

        public async Task<ControlMetadata> InsertControlMetadata(ControlMetadata control)
        {
            return await _controlsDataProvider.InsertControlMetadataAsync(control);
        }

        public async Task<IEnumerable<ControlMetadata>> GetControlsMetadataByControlSetAsync(string controlSetName)
        {
            var controlSet = await _controlSetManager.GetControlSetByNameAsync(controlSetName);

            var controls = new List<ControlMetadata>();
            foreach (var controlDefinition in controlSet.Controls) // query per entry. not very good but MEH!
            {
                if (string.IsNullOrEmpty(controlDefinition.Version))
                {
                    controls.Add(await _controlsDataProvider.GetLatestControlMetadataByNameAsync(controlDefinition.Name));
                } else
                {
                    controls.Add(await _controlsDataProvider.GetControlMetadataByNameAndVersionAsync(controlDefinition.Name, controlDefinition.Version));
                }
            }
            return controls;
        }

        public async Task<ControlMetadata> GetLatestControlMetadataByNameAsync(string name)
        {
            return await _controlsDataProvider.GetLatestControlMetadataByNameAsync(name);
        }

        public async Task<ControlMetadata> GetControlMetadataByNameAndVersionAsync(string name, string version)
        {
            return await _controlsDataProvider.GetControlMetadataByNameAndVersionAsync(name, version);
        }

        public async Task<ControlMetadata> GetControlMetadataByNameAndSemanticVersionAsync(string name, string version)
        {
            return await _controlsDataProvider.GetControlMetadataByNameAndSemanticVersionAsync(name, version);
        }        

        public async Task<IDictionary<string, ControlMetadata>> GetAllControlMetadataVersionsByNameAsync(string name)
        {
            var list = await _controlsDataProvider.GetAllControlMetadataVersionsByNameAsync(name);
            return list.ToDictionary(k => k.RuntimeMetadata.Version, v => v);
        }
    }
}
