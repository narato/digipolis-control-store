﻿using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces;
using System.Threading.Tasks;
using AutoMapper;
using System;
using System.Linq;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Managers
{
    public class ControlSetManager : IControlSetManager
    {
        private readonly IControlSetDataProvider _controlSetDataProvider;

        public ControlSetManager(IControlSetDataProvider controlSetDataProvider)
        {
            _controlSetDataProvider = controlSetDataProvider;
        }

        public async Task<ControlSet> InsertControlSetAsync(ControlSet controlSet)
        {
            if (await _controlSetDataProvider.ControlSetExistsByName(controlSet.Name))
            {
                throw new ValidationException(FeedbackItem.CreateValidationErrorFeedbackItem("This controlset aleady exists"));
            }

            return await _controlSetDataProvider.InsertControlSetAsync(controlSet);
        }

        public async Task<ControlSet> GetControlSetByNameAsync(string name)
        {
            return await _controlSetDataProvider.GetControlSetByNameAsync(name);
        }

        public async Task<ControlSet> GetControlSetByIdAsync(Guid id)
        {
            return await _controlSetDataProvider.GetControlSetByIdAsync(id);
        }

        public async Task<ControlSet> UpdateControlSetAsync(ControlSet controlSet, Guid id)
        {
            if (controlSet.Id != id)
            {
                throw new ValidationException(FeedbackItem.CreateValidationErrorFeedbackItem("Controlset Id doesn't match id in route"));
            }
            var currentControlSet = await _controlSetDataProvider.GetControlSetByIdAsync(id);
            if (currentControlSet.Name != controlSet.Name &&  await _controlSetDataProvider.ControlSetExistsByName(controlSet.Name))
            {
                throw new ValidationException(FeedbackItem.CreateValidationErrorFeedbackItem("Cannot rename a ControlSet to a name that already exists."));
            }

            var faultyControls = await _controlSetDataProvider.GetControlsAlreadyInOtherControlSets(controlSet);
            if (faultyControls.Count() > 0)
            {
                throw new ValidationException(FeedbackItem.CreateValidationErrorFeedbackItem("Cannot update this control. Following control Id's already exist under another controlset: " + string.Join(", ", faultyControls.Select(c => c.Id)) + "."));
            }

            await _controlSetDataProvider.DeleteControlSetById(id); // we do delete - create rather than update

            return await _controlSetDataProvider.InsertControlSetAsync(controlSet);
        }
    }
}
