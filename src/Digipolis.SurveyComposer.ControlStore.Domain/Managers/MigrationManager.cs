﻿using Digipolis.SurveyComposer.ControlStore.Common.Configurations;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Microsoft.Extensions.Options;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Managers
{
    public class MigrationManager : IMigrationManager
    {
        private readonly DatabaseConfiguration _databaseConfiguration;
        private readonly IQueryExecutor _queryExecutor;

        public MigrationManager(IOptions<DatabaseConfiguration> databaseConfiguration, IQueryExecutor queryExecutor)
        {
            _databaseConfiguration = databaseConfiguration.Value;
            _queryExecutor = queryExecutor;
        }

        public string ExecutePendingMigrations()
        {
            try
            {
                string returnMessage = "Nothing was updated";
                var builder = new NpgsqlConnectionStringBuilder(_databaseConfiguration.ConnectionString);
                var databaseName = builder.Database; // The target database
                builder.Database = "postgres"; // The postgres db should always be present

                bool dbExists;
                dbExists = _queryExecutor.FindDatabase(builder.ConnectionString, databaseName) != null;

                if (dbExists == false)
                {
                    _queryExecutor.CreateDatabase(builder.ConnectionString, databaseName);
                }

                builder.Database = databaseName;

                returnMessage = _queryExecutor.MigrateDatabase();

                return returnMessage;
            }
            catch (Exception e)
            {
                //TODO: logging?
                throw e;
            }
        }
    }
}
