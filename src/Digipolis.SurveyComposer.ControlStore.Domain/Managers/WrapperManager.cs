﻿using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Narato.Common.Exceptions;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Managers
{
    public class WrapperManager : IWrapperManager
    {
        private readonly IWrapperDataProvider _wrapperDataProvider;

        public WrapperManager(IWrapperDataProvider wrapperDataProvider)
        {
            _wrapperDataProvider = wrapperDataProvider;
        }

        public async Task<FileResult> GetWrapperByNameAsync(string name)
        {
            if (! _wrapperDataProvider.WrapperDirectoryExists(name))
            {
                throw new EntityNotFoundException($"Wrapper with name {name} couldn't be found.");
            }

            var fileInfo = _wrapperDataProvider.getWrapperFileByWrapperName(name);

            return new FileContentResult(await fileInfo.GetByteContentAsync(), fileInfo.GetContentType());
        }
    }
}
