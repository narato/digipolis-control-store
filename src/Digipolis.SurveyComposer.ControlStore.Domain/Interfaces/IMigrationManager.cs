﻿namespace Digipolis.SurveyComposer.ControlStore.Domain.Interfaces
{
    public interface IMigrationManager
    {
        string ExecutePendingMigrations();
    }
}
