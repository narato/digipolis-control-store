﻿using Digipolis.SurveyComposer.ControlStore.Model.Models;
using System;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Interfaces
{
    public interface IControlSetManager
    {
        Task<ControlSet> InsertControlSetAsync(ControlSet controlSet);

        Task<ControlSet> GetControlSetByNameAsync(string name);

        Task<ControlSet> GetControlSetByIdAsync(Guid id);

        Task<ControlSet> UpdateControlSetAsync(ControlSet controlSet, Guid id);
    }
}
