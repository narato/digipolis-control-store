﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Interfaces
{
    public interface IWrapperManager
    {
        Task<FileResult> GetWrapperByNameAsync(string name);
    }
}
