﻿using Digipolis.SurveyComposer.ControlStore.Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Interfaces
{
    public interface IControlsManager
    {
        Task<IEnumerable<ControlMetadata>> GetLatestControlsMetadataAsync();

        Task<ControlMetadata> InsertControlMetadata(ControlMetadata control);

        Task<IEnumerable<ControlMetadata>> GetControlsMetadataByControlSetAsync(string controlSetName);

        Task<ControlMetadata> GetLatestControlMetadataByNameAsync(string name);

        Task<ControlMetadata> GetControlMetadataByNameAndVersionAsync(string name, string version);

        Task<ControlMetadata> GetControlMetadataByNameAndSemanticVersionAsync(string name, string version);

        Task<IDictionary<string, ControlMetadata>> GetAllControlMetadataVersionsByNameAsync(string name);
    }
}
