﻿using Digipolis.SurveyComposer.ControlStore.Domain.ActionFilters;
using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ControlStoreApi.Controllers
{
    [Route("api/controls")]
    public class ControlsController : Controller
    {
        private readonly IControlsManager _controlsManager;
        private readonly IResponseFactory _responseFactory;

        public ControlsController(IControlsManager controlsManager, IResponseFactory responseFactory)
        {
            _controlsManager = controlsManager;
            _responseFactory = responseFactory;
        }

        /// <summary>
        /// Gets the latest versions of all controls.
        /// </summary>
        /// <param name="controlSetName">optionally the controlset which to fetch all control definitions for</param>
        /// <returns>the latest versions of the controls</returns>
        [ProducesResponseType(typeof(Response<IEnumerable<ControlMetadata>>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpGet]
        public async Task<IActionResult> GetAllAsync([FromQuery] string controlSetName)
        {
            if (string.IsNullOrEmpty(controlSetName))
            {
                return await _responseFactory.CreateGetResponseAsync(async () => await _controlsManager.GetLatestControlsMetadataAsync(), this.GetRequestUri());
            }
            return await _responseFactory.CreateGetResponseAsync(async () => await _controlsManager.GetControlsMetadataByControlSetAsync(controlSetName), this.GetRequestUri());
        }

        [CustomAuthHeaderActionFilter]
        [ApiExplorerSettings(IgnoreApi = true)] // this is only used by the admin api... Don't expose this in the swagger
        [HttpPost]
        public async Task<IActionResult> InsertControl([FromBody] ControlMetadata control)
        {
            return await _responseFactory.CreatePostResponseAsync(async () => await _controlsManager.InsertControlMetadata(control), this.GetRequestUri());
        }

        /// <summary>
        /// Gets the latest version of a control by key.
        /// </summary>
        /// <param name="key">The key of the control</param>
        /// <returns>the latest version of the control</returns>
        [ProducesResponseType(typeof(Response<ControlMetadata>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpGet("{key}")]
        public async Task<IActionResult> GetLatestByKeyAsync(string key)
        {
            return await _responseFactory.CreateGetResponseAsync(async () => await _controlsManager.GetLatestControlMetadataByNameAsync(key), this.GetRequestUri());
        }

        /// <summary>
        /// Gets all versions of a control by key. <br />
        /// Returns A Dictionary containing key-value pairs where the key is the version and the value is a Control. <br />
        /// This is correctly defined in the swagger json file, but swagger UI doesn't support MAP types yet apparently. <br />
        /// see https://github.com/swagger-api/swagger-ui/issues/1248 
        /// </summary>
        /// <param name="key">The key of the control</param>
        /// <returns>A Dictionary containing key-value pairs where the key is the version and the value is a Control.</returns>
        [ProducesResponseType(typeof(Response<IDictionary<string, ControlMetadata>>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpGet("{key}/versions")]
        public async Task<IActionResult> GetAllVersionsByKeyAsync(string key)
        {
            return await _responseFactory.CreateGetResponseAsync(async () => await _controlsManager.GetAllControlMetadataVersionsByNameAsync(key), this.GetRequestUri());
        }

        /// <summary>
        /// Gets a specific version of a control by key.
        /// </summary>
        /// <param name="key">The key of the control</param>
        /// <param name="version">The version of the control</param>
        /// <returns>the control with this version</returns>
        [ProducesResponseType(typeof(Response<ControlMetadata>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpGet("{key}/versions/{version}")]
        public async Task<IActionResult> GetControlByKeyAndSemanticVersionAsync(string key, string version)
        {
            return await _responseFactory.CreateGetResponseAsync(async () => await _controlsManager.GetControlMetadataByNameAndSemanticVersionAsync(key, version), this.GetRequestUri());
        }
    }
}
