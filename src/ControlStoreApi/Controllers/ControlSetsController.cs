﻿using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace ControlStoreApi.Controllers
{
    [Route("api/controlsets")]
    public class ControlSetsController : Controller
    {
        private readonly IResponseFactory _responseFactory;
        private readonly IControlSetManager _controlSetManager;

        public ControlSetsController(IResponseFactory responseFactory, IControlSetManager controlSetManager)
        {
            _responseFactory = responseFactory;
            _controlSetManager = controlSetManager;
        }

        /// <summary>
        /// Gets a controlset by name
        /// </summary>
        /// <param name="name">name of the controlset</param>
        /// <returns>the controlset</returns>
        [ProducesResponseType(typeof(Response<ControlSet>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpGet("name/{name}")]
        public async Task<IActionResult> GetByNameAsync(string name)
        {
            return await _responseFactory.CreateGetResponseAsync(async () => await _controlSetManager.GetControlSetByNameAsync(name), this.GetRequestUri());
        }

        /// <summary>
        /// Gets a controlset by id
        /// </summary>
        /// <param name="id">id of the controlset</param>
        /// <returns>the controlset</returns>
        [ProducesResponseType(typeof(Response<ControlSet>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpGet("{id}", Name = "GetControlSetById")]
        public async Task<IActionResult> GetByIdAsync(Guid id)
        {
            return await _responseFactory.CreateGetResponseAsync(async () => await _controlSetManager.GetControlSetByIdAsync(id), this.GetRequestUri());
        }

        /// <summary>
        /// inserts a new controlset
        /// </summary>
        /// <param name="controlSet">the controlset to insert</param>
        /// <returns>the controlset</returns>
        [ProducesResponseType(typeof(Response<ControlSet>), (int)System.Net.HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody][Required]ControlSet controlSet)
        {
            Func<ControlSet, object> pathMapper = (data) =>
            {
                return new { data.Id };
            };
            return await _responseFactory.CreatePostResponseAsync(async () => await _controlSetManager.InsertControlSetAsync(controlSet), this.GetRequestUri(),
                "GetControlSetById", pathMapper);
        }

        /// <summary>
        /// updates an existing controlset
        /// </summary>
        /// <param name="controlSet">the controlset to update</param>
        /// <param name="id">the id of the existing controlset</param>
        /// <returns>the controlset</returns>
        [ProducesResponseType(typeof(Response<ControlSet>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync([FromBody][Required]ControlSet controlSet, Guid id)
        {
            return await _responseFactory.CreatePutResponseAsync(async () => await _controlSetManager.UpdateControlSetAsync(controlSet, id), this.GetRequestUri());
        }
    }
}
