﻿using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Interfaces;
using Narato.Common.Models;
using System;
using System.Threading.Tasks;

namespace ControlStoreApi.Controllers
{
    [Route("api/wrappers")]
    public class WrappersController : Controller
    {
        private readonly IWrapperManager _wrapperManager;
        private readonly IExceptionToActionResultMapper _exceptionMapper;

        public WrappersController(IWrapperManager wrapperManager, IExceptionToActionResultMapper exceptionMapper)
        {
            _wrapperManager = wrapperManager;
            _exceptionMapper = exceptionMapper;
        }

        /// <summary>
        /// Gets a wrapper by name
        /// </summary>
        /// <param name="name">name of the wrapper</param>
        /// <returns>the wrapper</returns>
        [ProducesResponseType(typeof(string), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpGet("{name}")]
        public async Task<IActionResult> GetWrapperByName(string name)
        {
            try
            {
                return await _wrapperManager.GetWrapperByNameAsync(name);
            }
            catch (Exception ex)
            {
                return _exceptionMapper.Map(ex, this.GetRequestUri());
            }
        }
    }
}
