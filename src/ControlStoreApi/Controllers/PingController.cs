﻿using Digipolis.SurveyComposer.ControlStore.Model.Models.Monitoring;
using Microsoft.AspNetCore.Mvc;

namespace ControlStoreApi.Controllers
{
    [Route("api/status/")]
    public class PingController : Controller
    {

        /// <summary>
        /// This is a heartbeat endpoint. If you get a response, you know the engine is up
        /// </summary>
        /// <returns>a ping object, proving that the server is accessible</returns>
        [ProducesResponseType(typeof(Ping), (int)System.Net.HttpStatusCode.OK)]
        [HttpGet("ping")]
        public IActionResult Get()
        {
            var ping = new Ping
            {
                Status = "ok"
            };
            return new OkObjectResult(ping);
        }
    }
}
