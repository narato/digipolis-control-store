﻿using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;

namespace ControlStoreApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/migrations")]
    public class MigrationsController : Controller
    {
        private IMigrationManager _migrationManager;
        private IResponseFactory _responseFactory;

        public MigrationsController(IMigrationManager migrationManager, IResponseFactory responseFactory)
        {
            _migrationManager = migrationManager;
            _responseFactory = responseFactory;
        }

        [HttpPost]
        public IActionResult Post()
        {
            return _responseFactory.CreateGetResponse(() => _migrationManager.ExecutePendingMigrations(), this.GetRequestUri());
        }
    }
}
