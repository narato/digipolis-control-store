﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Narato.Common.ActionFilters;
using Newtonsoft.Json.Serialization;
using Narato.Common.DependencyInjection;
using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Domain.Managers;
using Digipolis.SurveyComposer.ControlStore.DataProvider.DataProviders;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Common.Configurations;
using Microsoft.EntityFrameworkCore;
using Digipolis.SurveyComposer.ControlStore.DataProvider.QueryProviders;
using Digipolis.SurveyComposer.ControlStore.DataProvider.Contexts;
using System.Collections.Generic;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using Swashbuckle.Swagger.Model;
using System;

namespace ControlStoreApi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile("config.json")
                .AddJsonFile("config.json.local", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ControlStoreConfiguration>(Configuration.GetSection("ControlStoreConfiguration"));
            services.Configure<DatabaseConfiguration>(Configuration.GetSection("DatabaseConfiguration"));
            // Add framework services.
            services.AddMvc(
                //Add this filter globally so every request runs this filter to recored execution time
                config =>
                {
                    config.Filters.Add(new ExecutionTimingFilter());
                    config.Filters.Add(new ModelValidationFilter());
                })
                //Add formatter for JSON output to client and to format received objects         
                .AddJsonOptions(x =>
                {
                    x.SerializerSettings.ContractResolver =
                     new CamelCasePropertyNamesContractResolver();
                }
            );

            services.AddCors();

            services.AddEntityFrameworkNpgsql();
            services.AddDbContext<ControlStoreContext>(options => options.UseNpgsql(Configuration["DatabaseConfiguration:ConnectionString"]));

            services.AddTransient<IControlsManager, ControlsManager>();
            services.AddTransient<IControlDataProvider, ControlDataProvider>();

            services.AddTransient<IWrapperManager, WrapperManager>();
            services.AddTransient<IWrapperDataProvider, WrapperDataProvider>();

            services.AddTransient<IControlSetManager, ControlSetManager>();
            services.AddTransient<IControlSetDataProvider, ControlSetDataProvider>();

            services.AddSwaggerGen();
            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Contact = new Contact { Name = "Narato NV" },
                    Description = "ControlStore API",
                    Version = "v1",
                    Title = "ControlStore"
                });
                //options.OperationFilter<ProducesConsumesFilter>();

                var xmlPaths = GetXmlCommentsPaths();
                foreach (var entry in xmlPaths)
                {
                    try
                    {
                        options.IncludeXmlComments(entry);
                    }
                    catch (Exception e)
                    {
                    }
                }
            });

            services.AddNaratoCommon();

            // auto migration
            services.AddTransient<IMigrationQueryProvider, MigrationQueryProvider>();
            services.AddTransient<IQueryExecutor, QueryExecutor>();
            services.AddTransient<IMigrationManager, MigrationManager>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            //loggerFactory.AddNLog();

            //env.ConfigureNLog("nlog.config");

            app.UseCors(builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUi();

            app.UseMvc();
        }

        private List<string> GetXmlCommentsPaths()
        {
            var app = PlatformServices.Default.Application;
            var files = new List<string>()
                        {
                            "ControlStoreApi.xml"
                        };

            List<string> paths = new List<string>();
            foreach (var file in files)
            {
                paths.Add(Path.Combine(app.ApplicationBasePath, file));
            }

            return paths;
        }
    }
}
